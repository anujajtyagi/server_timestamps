provider "aws" {
  region = "us-east-1"  # Update with your desired region
}

provider "gitlab" {
  token = ""
  base_url = "https://gitlab.com"
}
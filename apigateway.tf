provider "aws" {
  region = "us-east-1"  # Update with your desired region
}

resource "aws_api_gateway_rest_api" "timestamp_api" {
  name = "recent_timestamp"
  description = "REST API to get recent timestamp"
  # endpoint_configuration {
  #   types = ["REGIONAL"]
  # }
  depends_on = [ 
    aws_lambda_function.s3_object_decrypt
   ]
}

# To get api gateway id
resource "aws_api_gateway_resource" "proxy_resource" {
  path_part = "timestamp"
  rest_api_id = aws_api_gateway_rest_api.timestamp_api.id
  parent_id   = aws_api_gateway_rest_api.timestamp_api.root_resource_id
  
}

# enable api gateway method
resource "aws_api_gateway_method" "proxy_method" {
  rest_api_id   = aws_api_gateway_rest_api.timestamp_api.id
  resource_id   = aws_api_gateway_resource.proxy_resource.id
  http_method   = "GET"
  authorization = "NONE"
  # api_key_required = true
}


resource "aws_api_gateway_method_settings" "api_method_settings" {
  rest_api_id = aws_api_gateway_rest_api.timestamp_api.id
  stage_name  = "prod"
  method_path = "${aws_api_gateway_resource.timestamp_api.path_part}/${aws_api_gateway_method.proxy_method.http_method}"
  settings {
    throttling_burst_limit = 210
    throttling_rate_limit  = 210
  }
}

resource "aws_api_gateway_stage" "prod_stage" {
  stage_name = "prod"
  rest_api_id = aws_api_gateway_rest_api.timestamp_api.id
  deployment_id = aws_api_gateway_deployment.api_gateway_deployment_get.id
}


## usage plan
# resource "aws_api_gateway_usage_plan" "apigw_usage_plan" {
#   name = "apigw_usage_plan"

#   api_stages {
#     api_id = aws_api_gateway_rest_api.screenshot_api.id
#     stage = aws_api_gateway_stage.prod_stage.stage_name
#   }
# }

# resource "aws_api_gateway_usage_plan_key" "apigw_usage_plan_key" {
#   key_id = aws_api_gateway_api_key.apigw_prod_key.id
#   key_type = "API_KEY"
#   usage_plan_id = aws_api_gateway_usage_plan.apigw_usage_plan.id
# }

# resource "aws_api_gateway_api_key" "apigw_prod_key" {
#   name = "prod_key"
# }


### permission to api gw to invoke lambda function
resource "aws_lambda_permission"  "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_object_decrypt.arn
  principal =  "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.get_timestamp.execution.arn}/*"
}

### api gw and lambda func integration
resource "aws_api_gateway_integration" "lambda_integration_get" {

  depends_on = [  
    aws_lambda_permission.apigw
  ]

  rest_api_id             = aws_api_gateway_rest_api.timestamp_api.id
  resource_id             = aws_api_gateway_resource.proxy_resource.id
  http_method             = aws_api_gateway_method.proxy_method.http_method
  integration_http_method = "POST" # https://github.com/hashicorp/terraform/issues/9271 Lambda requires POST as the integration type
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.s3_object_decrypt.invoke_arn
}

## deploy api gateway
resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [aws_api_gateway_integration.lambda_integration_get]
  rest_api_id = aws_api_gateway_rest_api.timestamp_api.id
  stage_name = "prod"
}


# resource "aws_api_gateway_account" "apigw_account" {
#   cloudwatch_role_arn = aws_iam_role.apigw_cloudwatch.arn
# }

# resource "aws_iam_role" "apigw_cloudwatch" {
#   # https://gist.github.com/edonosotti/6e826a70c2712d024b730f61d8b8edfc
#   name = "api_gateway_cloudwatch_global"
#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Sid": "",
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "apigateway.amazonaws.com"
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# EOF
# }

# resource "aws_iam_role_policy" "apigw_cloudwatch" {
#   name = "default"
#   role = aws_iam_role.apigw_cloudwatch.id

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "logs:CreateLogGroup",
#                 "logs:CreateLogStream",
#                 "logs:DescribeLogGroups",
#                 "logs:DescribeLogStreams",
#                 "logs:PutLogEvents",
#                 "logs:GetLogEvents",
#                 "logs:FilterLogEvents"
#             ],
#             "Resource": "*"
#         }
#     ]
# }
# EOF
# }



# resource "aws_api_gateway_usage_plan" "main" {
#   name = "usage-plan"
# }

# resource "aws_api_gateway_api_key" "main" {
#   name = "api-key"
# }

# resource "aws_api_gateway_usage_plan_key" "main" {
#   key_id        = aws_api_gateway_api_key.main.id
#   key_type      = "API_KEY"
#   usage_plan_id = aws_api_gateway_usage_plan.main.id
# }

# resource "aws_api_gateway_usage_plan_quota" "main" {
#   usage_plan_id = aws_api_gateway_usage_plan.main.id
#   limit         = 210
#   offset        = 1
#   period        = "DAY" # This is where you specify the period for rate limiting
#   period_seconds = 600  # 10 minutes
# }


# resource "aws_lambda_permission" "apigw" {
#   statement_id  = "AllowExecutionFromAPIGateway"
#   action        = "lambda:InvokeFunction"
#   function_name = aws_lambda_function.s3_retrieval_lambda.function_name
#   principal     = "apigateway.amazonaws.com"
#   source_arn    = "arn:aws:execute-api:${var.aws_region}:${var.aws_account}:${aws_api_gateway_rest_api.my_api.id}/*/*"
# }

resource "aws_kms_key" "kms_s3_encrypt" {
  description             = "kms_s3_encrypt"
  deletion_window_in_days = -1  # Set the retention period for deleted keys (optional)
  enable_key_rotation     = false  # Enable key rotation for increased security (optional)
}

# 1. Provide diagram of the implementation

![image info](images/timestamp.jpg)


# 2. Provide terraform code

```
Use this repo as TF Module: https://gitlab.com/anujajtyagi/server_timestamps.git
```

# 3. Provide a brief summary of the implemented solution including:

○ Why the services/components were chosen over alternatives
```
a. AWS Lambda: 
- To push timestamp every 10 minutes and push to S3 buckets after encrypt with KMS, I executed Python code from AWS Lambda service since source of timestamp was not mentioned in ther requirements. If we want to get timestamp of any server like EC2, we can run same code so no changes required.

- To read the plain text format of the encrypted object by decrypting it, AWS Lambda service is used again with a different function everytime customer send request, API Gateway invoke Lambda function

- To rate limit user request with same IP, I have used another code with AWS Lambda function to get the customer IP from the WAF request

b. API Gateway: 

- To create API endpoint and provide url for user to send GET request in order to get the plaintext of timestamp, API Gateway service is used 

c. KMS: As mentioned in the required use KMS to encrypt S3 bucket objects

d. SNS: To send email notification for alerts since this service seems to be apt for this requirement

e. CloudWatch: 
- To create alert for rate limit exceed
- To get logs
```
# 4. How the cost of the implementation scales as traffic increases
```
a. API Gateway: Free tier provide 1 Million HTTP API calls and after that it's $1 per million requests. 
b. AWS Lambda: Free tier provide: 1 million free requests per month, 400k GB/Seconds of computer time per month, and 100 GiB of HTTP response streaming per month. After that, it's $0.0000166667 for every GB-second and $0.20 per 1M requests.
```
c. S3: 
```
- $0.023 per GB for standard tiering
but we can switch to Intelligent tiering by adding a lifecycle rule which can archive infrequent used objects of bucket. This can reduce the cost. Although it's applicable only when object size is larger than 128 KB so we better keep 30 minute as retention rule to delete old objects. 
```

# 5.  How to monitor the implementation for availability and performance.
```
- We can monitor all AWS services in use using the metrics data being received on AWS CloudWatch. We can make Dashboards in Grafana using data source as CloudWatch. This way, everyone don't need to login to AWS console in order to check the infrastructure performance. 
```
# Important metrics to monitor the availability and performance: 
1. API Gateway: 
```
a. Count: The total number API requests in a given period.
b. Latency: The time between when API Gateway receives a request from a client and when it returns a response to the client. The latency includes the integration latency and other API Gateway overhead.
c. CacheHitCount: The number of requests served from the API cache in a given period.
d. 5XXError: The number of server-side errors captured in a given period.
e. 4xxError: The number of client-side errors captured in a given period.
```
2.  AWS Lambda: 
```
Invocations: Monitor this value as a general barometer for the amount of traffic flowing through your serverless application

Duration: This is the amount of time taken for a Lambda invocation
Errors: This logs the number of errors thrown by a function

Throttles: Set alarms on this metric for any non-zero value since this only occurs if the number of invocations exceeds concurrency in your account

ConcurrentExecutions: monitor this value to ensure that your functions are not running close to the total concurrency limit
```


# How to recover from a regional disaster and impact to end users.

1. AWS Lambda and API Gateway: 
```
- Use a regional API Gateway and associated Lambda functions in each region
- Use Route 53 latency or failover routing with health checks in front API Gateways
```
2. AWS S3: 
```
- Use versioning and cross region replication for S3 buckets
- Use CloudFront origin failover for read access to replicated S3 buckets
```
3. Add CDN to scale globally and add high availability
Example: 
![image info](images/example.jpg)

# How the implementation complies with best practices (AWS Well Architected Framework or Google Cloud Architecture Framework.)

1. Operational Excellence Pillar: to monitor the service for performance

2. Security Pillar: 
- Stores the timestamp files encrypted with a KMS key
-  Implented IAM roles and policies implemented to restrict access to required  resources only

3. Cost Optimization Pillar: 
- Object Lifecycle Management to automatically delete old timestamp files from the S3 bucket

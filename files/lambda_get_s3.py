# Lambda function to get most recent S3 object and output as in plain text

"""
This is module which contains all classes related to aws S3
"""
"""
    awshelper.py
    -------
    This module contains the AWS class
"""
import boto3
from datetime import datetime
import os 
import json
import uuid
import base64
 
 
"""
 
{
	"Version": "2012-10-17",
	"Id": "PutObjPolicy",
	"Statement": [
		{
			"Sid": "DenyIncorrectEncryptionHeader",
			"Effect": "Deny",
			"Principal": "*",
			"Action": "s3:PutObject",
			"Resource": "arn:aws:s3:::test43234/*",
			"Condition": {
				"StringNotEquals": {
					"s3:x-amz-server-side-encryption": "aws:kms"
				}
			}
		},
		{
			"Sid": "DenyUnEncryptedObjectUploads",
			"Effect": "Deny",
			"Principal": "*",
			"Action": "s3:PutObject",
			"Resource": "arn:aws:s3:::test43234/*",
			"Condition": {
				"Null": {
					"s3:x-amz-server-side-encryption": "true"
				}
			}
		}
	]
}
 
"""
 
 
 
def not_secure_way():
 
    client = boto3.client(
        "s3",
        aws_access_key_id="AKIAROU23JJKZBNWXA3V",
        aws_secret_access_key="6Uxw6oXri5V9ZLf56bMjT64Xx+kl9Axx6569eGzK",
        region_name="us-east-1",
    )
 
    #faker = Faker()
    current_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    data = {"time": current_time}
    id = uuid.uuid4().__str__()
 
    response = client.put_object(
        ACL="private",
        Body=bytes(json.dumps(data).encode("UTF-8")),
        Bucket="test43234", Key=f"{uuid.uuid4().__str__()}.json",
 
    )
 
    print(response)
 
def secure():
 
    s3_client = boto3.client(
        "s3",
        aws_access_key_id="AKIAROU23JJKZBNWXA3V",
        aws_secret_access_key="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",
        region_name="us-east-1",
    )
 
    kms = boto3.client(
        "kms",
        aws_access_key_id="AKIAROU23JJKZBNWXA3V",
        aws_secret_access_key="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",
        region_name="us-east-1",
    )
 
    # =====================================================
    #                    DATA to Upload
    #=====================================================
    #faker = Faker()
    #data = {"name":faker.name(), "address":faker.address()}
 
    current_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    data = {"time": current_time}
 
 
    # =====================================================
    #                    CIPHER
    #=====================================================
 
    ciphertext = kms.encrypt(
        KeyId='ec893c86-7534-42f4-a4db-a5f070722a02',
        Plaintext=json.dumps(data)
    )
    encoded_cipher_text = base64.b64encode(ciphertext["CiphertextBlob"])
    print(ciphertext)
 
    response = s3_client.put_object(
        Bucket="test43234",
        Key=f"{uuid.uuid4().__str__()}.json",
        Body=encoded_cipher_text,
        ServerSideEncryption='aws:kms',
        SSEKMSKeyId='ec893c86-7534-42f4-a4db-a5f070722a02',
    )
    print(response)
 
if __name__ == "__main__":
    secure()
 
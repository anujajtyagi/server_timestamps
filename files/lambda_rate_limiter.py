import boto3
import json

def lambda_handler(event, context):
    # Extract the client IP address from the event
    client_ip = event['requestContext']['identity']['sourceIp']

    # Check the request count for the client IP from a data store (e.g., DynamoDB)
    request_count = get_request_count(client_ip)
    
    # If the request count exceeds the limit, deny the request
    if request_count >= 210:
        return generate_policy("Deny", client_ip)
    else:
        return generate_policy("Allow", client_ip)

def get_request_count(client_ip):
    # Implement logic to retrieve request count for the client IP from a data store
    # This could involve querying a DynamoDB table or another storage solution
    pass

def generate_policy(effect, client_ip):
    policy = {
        "principalId": client_ip,
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [{
                "Action": "execute-api:Invoke",
                "Effect": effect,
                "Resource": "*"
            }]
        }
    }
    return policy

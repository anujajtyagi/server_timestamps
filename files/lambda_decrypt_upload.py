import boto3
from datetime import datetime
import os 
import json
import uuid
import base64


def lambda_handler(event, context):
    # Get environment variables
    #bucket_name = os.environ['BUCKET_NAME']
    #kms_key_arn = os.environ['KMS_KEY_ARN']
    
    # Create S3 client
    s3_client = boto3.client('s3')
    kms = boto3.client('kms')
    # Generate current timestamp
    # current_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    # data = {"time": current_time}
    
    current_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    object_key = f"{current_time}.txt"
    object_data = current_time
    # Create object with current timestamp

    response = kms.encrypt(
        KeyId='ec893c86-7534-42f4-a4db-a5f070722a02',
        #Plaintext=json.dumps(data)
        Plaintext=object_data.encode('utf-8')
    )
    #encoded_cipher_text = base64.b64encode(ciphertext["CiphertextBlob"])
    encrypted_data = response['CiphertextBlob']
    print(encrypted_data)
 
    response = s3_client.put_object(
        Bucket="test43234",
        Key=object_key,
        Body=encrypted_data,
        ServerSideEncryption='aws:kms',
        SSEKMSKeyId='ec893c86-7534-42f4-a4db-a5f070722a02',
    )
    
    return {
        'statusCode': 200,
        'body': 'Success'
    }

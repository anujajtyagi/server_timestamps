resource "aws_sns_topic" "api_topic" {
  name = "timestamp-api-topic"
}

resource "aws_sns_topic_subscription" "api_notification_subscription" {
  topic_arn = aws_sns_topic.api_topic.arn
  protocol  = "email"
  endpoint  = "example@example.com"  # Replace with your email address
}
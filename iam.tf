resource "aws_iam_role" "lambda_s3_upload" {
  name = "lambda_s3_upload"
  assume_role_policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
)

resource "aws_iam_role_policy" "kms_s3_encrypt" {
  name = "kms_s3_encrypt"
  role = aws_iam_role.lambda_s3_upload.id
  policy = jsonencode(
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VolumeEncryption",
            "Effect": "Allow",
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:DescribeKey",
                "kms:GenerateDataKey"
            ],
            "Resource": "arn:aws:kms:us-east-1:100182739541:key/*"
        }
    ]
}
  )
}

# Do not use aws_iam_policy since those are exclusive to the 
resource "aws_iam_role_policy_attachment" "lambda_s3_upload_kms" {
  role = aws_iam_role.lambda_s3_upload.name
  policy_arn = aws_iam_role_policy.kms_s3_encrypt.arn
}

# attach access on S3 to upload buckets
resource "aws_iam_role_policy_attachment" "lambda_s3_upload_access" {
  role = aws_iam_role.lambda_s3_upload.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}



resource "aws_iam_role" "lambda_read_bucket" {
  name = "lambda_read_bucket"
  assume_role_policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
)

resource "aws_iam_role_policy_attachment" "lambda_s3_upload_kms" {
  role = aws_iam_role.lambda_read_bucket.name
  policy_arn = aws_iam_role_policy.kms_s3_encrypt.arn
}

# attach access on S3 to upload buckets
resource "aws_iam_role_policy_attachment" "lambda_s3_upload_access" {
  role = aws_iam_role.lambda_read_bucket.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
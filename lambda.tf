
resource "aws_lambda_function" "s3_upload" {
  filename      = "${path.module}/files/s3_upload.zip"  # Path to your Lambda deployment package
  function_name = "s3_upload"
  handler       = "index.handler"
  runtime       = "python3.12"
  role          = aws_iam_role.lambda_s3_upload.arn
  source_code_hash = filebase64sha256("${path.module}/files/s3_object_decrypt.zip")
  timeout = 10
  environment {
    variables = {
      BUCKET_NAME = "server_timestamps"  # Update with your S3 bucket name
      KMS_KEY_ARN = "arn:aws:kms:us-east-1:100182739541:key/ec893c86-7534-42f4-a4db-a5f070722a02"   # Update with your KMS key ARN
    }
  }
  depends_on = [
    # make sure s3 bucket and kms key is created
    aws_s3_bucket.name
    aws_kms_key.arn
  ]
}

resource "aws_lambda_function" "s3_object_decrypt" {
  filename      = "${path.module}/files/s3_object_decrypt.zip"  # Path to your Lambda deployment package
  function_name = "s3_object_decrypt"
  handler       = "index.handler"
  runtime       = "python3.12"
  role          = aws_iam_role.lambda_read_bucket.arn
  source_code_hash = filebase64sha256("${path.module}/files/s3_object_decrypt.zip")
  timeout = 10
  environment {
    variables = {
      BUCKET_NAME = "server_timestamps"  # Update with your S3 bucket name
      KMS_KEY_ARN = "arn:aws:kms:us-east-1:100182739541:key/ec893c86-7534-42f4-a4db-a5f070722a02"   # Update with your KMS key ARN
    }
  }
}


resource "aws_cloudwatch_event_rule" "schedule" {
  name                = "s3-upload-schedule"
  description         = "Schedule for uploading S3 objects"
  schedule_expression = "rate(10 minutes)"
}

resource "aws_cloudwatch_event_target" "lambda_target" {
  rule      = aws_cloudwatch_event_rule.schedule.name
  target_id = "upload-lambda-target"
  arn       = aws_lambda_function.s3_upload_lambda.arn
}

provider "aws" {
  region = "us-east-1"  # Update with your desired region
}

# Create a CloudWatch Logs Group
resource "aws_cloudwatch_log_group" "api_gateway_logs" {
  name              = "/aws/api_gateway/logs"
  retention_in_days = 7
}


# Create a metric filter to count HTTP requests exceeding the rate limit
resource "aws_cloudwatch_log_metric_filter" "api_request_metric_filter" {
  name           = "RequestRateExceededFilter"
  pattern        = "{$.requestContext.identity.sourceIp, $.requestContext.requestId} RATE exceeded"
  log_group_name = aws_cloudwatch_log_group.api_gateway_logs.name

  metric_transformation {
    name      = "RequestRateExceededMetric"
    namespace = "AWS/ApiGateway"
    value     = "1"
  }
}

# Create a CloudWatch alarm to monitor the metric
resource "aws_cloudwatch_metric_alarm" "timestamp_alarm" {
  alarm_name          = "RateLimitExceededAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "RequestRateExceededMetric"
  namespace           = "AWS/ApiGateway"
  period              = 600  # 10 minutes
  statistic           = "Sum"
  threshold           = 210
  alarm_description   = "HTTP endpoint rate limit exceeded for more than 210 requests per IP every 10 minutes"
  alarm_actions       = ["arn:aws:sns:us-east-1:123456789012:timestamp-api-topic"]  # Replace with your SNS topic ARN
}
